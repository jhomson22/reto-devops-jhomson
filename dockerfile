#SE usa una imagen liviana de node ya que se busca crear un entorno ligero.
FROM node:current-alpine3.12
ENV NODE_ENV=production

#Se define el directorio en el entorno virtual donde se va a copiar los archivos de la aplicación.
WORKDIR /app

#Antes de que se ejecute la instrucción npm install copiamos los archivos que indican
#los archivos "package.json" y "package-lock.json" quienes indican los componentes que
#deben ser instalados para la correcta ejecución del aplicativo en el entorno virtual
#que se instalará en el directorio /app
COPY ["package.json", "package-lock.json*", "./"]

RUN npm install --production

#Se añade un Usuario diferente de root que no tenga los permisos del grupo administrador, así
#evitamos que el usuario pueda ejecutar comandos como "apt-get install".
RUN adduser -D --home /home/appuserhome appuser

#Se indica que el Usuario creado esta autorizado para correr el entorno virtual.
USER appuser

#Copimos todos los archivos de nuestro directorio a la carpeta de trabajo de nuestro entorno virtual.
COPY . .

#Indicamos el comando a ejecutar cuando la imagen creada este corriendo dentro de un contenedor.
CMD [ "node", "index.js" ]
