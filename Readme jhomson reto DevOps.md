Proceso de instalación de entorno de trabajo en Laptop HP Pavilion DV6

En primera instancia se instalko el sistema operativo Ubuntu 20.04 y se actualizaron todas sus dependencias, esto con la finalidad de brindar un entorno de trabajo adecuado. Se instalaron las herramientas de software necesarias:

Curl v7.68
Node v16.13.0
npm  v8.1.0
docker v20.10.7
docker-compose v2.1.0
OpenSSL v1.1.1
Git v2.25.1
Nginx v1.18
Nano v4.8
htpasswd

Comenzado con los retos:

1- El primer reto plantea el realizar un fork de la aplicación a mi cuenta gitlab, en este caso decidi usar el comando clone luego de efectuar el fork para efectuar mis pruebas a nivel local e ir efectuando cambios a medida que se va avanzando en el desarrollo de los retos.

Se ubico un directorio adecuado y se realizo el clone de la siguiente manera:

cd /home/jhomson/Documents/gitlab/reto-devops-jhomson
git clone https://gitlab.com/jhomson22/reto-devops-jhomson.git

Posteriormente, para cumplir con el primer reto, se creó un archivo "dockerfile" que contiene el conjunto de instruccioners necesarías para ejecutar la aplicación en un contenedor, este archivo esta ubicado en el directorio raiz de la aplicación en cuestión, dentro del archivo "dockerfile" se encuentra una breve explicación de cada comando, para hacer entender al lector el porque se uso cada instrucción.

Una vez establecidos los parámetros del archivo "dockerfile" se inicia esta primera prueba ejecutando en el terminal lo siguiente:

docker build -t reto-devops-jhomson-node .

Esta instrucción construye la imagen del contenedor usando los parametros establecidos en el archivo "dockerfile", como se pide la instalación mas liviana posible, he usado la versión node:alpine3.1.2 ademas de excluir algunos archivos con el archivo "dockerignore" como es el caso de la carpeta "node-modules".

Luego se ejecuta el siguiente comando:

docker run --user appuser -dp 3000:3000 reto-devops-jhomson

Esta instrucción da inicio al servicio previamente configurado desde el archivo "dockerfile", el parámetro --user establece que Usuario va a acceder al entorno virtual, en este caso, garantizamos que sea un Usuario sin permisos de Administrador, despues con el parametro -dp se establecen dos condiciones, la primera es la de correr en modo "detached", para que se ejecute dicho contenedor en segundo plano y poder retomar el control de la terminal, y la segunda establece el puerto usado por el contenedor para recibir solicitudes desde el exterior, en este caso, se trata del puerto 3000, por lo que se puede ejecutar una consulta curl

curl -i localhost:3000

Para verificar si el contenedor devuelve una respuesta valida, en caso contrario rechazará la conexión.

Otra forma de verificarlo es abriendo en el navegador la url "localhost:3000", al ingresar, se obtiene como respuesta la consulta hecha al aplicativo index.js-->"ApiRest prueba".

Al ingresar a las demas rutas declaradas en el aplicativo se obtiene lo siguiente:

localhost:3000/public --> public_token: "12837asd98a7sasd97a9sd7"

localhost:3000/private --> private_token: "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="

De esta forma queda demostrado que se ha "Dockerizado" la aplicación y que esta corre de fomra correcta dentro del sistema operativo Anfitrión.

Como último detalle, si desea visualizar las imagenes creadas en docker, debe ejecutar lo siguiente:

docker images --> Muestra una lista de las imagenes creadas con el comando docker build.

En este caso, la imagen resultante obtuvo un peso de 179mb, incluyendo todos los componentes y librerias de npm.

Además se tiene la opción de ver que contenedores se encuentran en ejecución en nuestro sistema operativo, usando el comando

docker ps

Si desea pausar alguna, debe usar el comando

docker stop "container_ID" --> Dicho ID se puede observar con el comando anterior.

Y si desea eliminar los contenedores activos, ejecute este comando:

docker kill :container_id

En caso de querer eliminar todos los contenedores, asi cmo imagnes y datos descargador, puede ejecutar el siguiente comando

docker system prune

Con esto doy por terminado el reto #1 :)

2- El reto #2, consiste en hacer uso de docker compose para ejecutar varios servicios en un mismo contenedor, todo esto con el fin de cumplir con algunos puntos.

- Nginx que funcione como proxy reverso a nuesta app Nodejs
- Asegurar el endpoint /private con auth_basic
- Habilitar https y redireccionar todo el trafico 80 --> 443

Uno de los primeros cambios que se realizaron para dar respuesta a este reto, fue crear un archivo "docker-compose.yml" en el directorio del proyecto, en este archivo se especifican una serie de parámetros con el fin de establecer que serviciós se van a instalar en el contenedor, que puertos seran habilitados, que volúmenes van a ser mapeados al contenbedor, así como que red va a ser usada a nivel interno para la comunicación entre los servicios que se ejecutaran en el contenedor.

Todas las instrucciones mencionadas, estan debidamente comentadas en el archivo "docker-compose.yml".

Las instrucciones necesarias para la configuración del endpoint cxon auth_basic, asi como para la redirección se encuentran debidamente comentadas en el archivo de configuración de nginx "nginx.conf" ubicado dentro de la carpeta "nginx-conf".

*Nota: Como se trata de un ejemplo aplicado a un servidor local, se va a usar un certificado creado con la utilidad OpenSSL, este cerfitifcado se crea usando el siguiente comando:

sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/api.publicdata.key -out /etc/ssl/certs/api.publicdata.crt

Y para la creación del acrchivo de claves he usado la utilidad htpasswd.

sudo htpasswd -c /etc/nginx/.htpasswd appuser


Una vez configuardas todas las intrucciones en los servicios correspondientes, se puede dar por cumplido el punto #2.

3- Sobre el punto #3, se creo una rama de nombre "test-master", donde se proceden a plicar los estandares establecidos hasta el momento para una metodología CI/CD, la cual incluye hacer pipelines que se encrguen de efectuar las revisiones correspondientes al codigo que suba cada colaborador, ejecutar ciertos test que permitan determinar si el codigo que se ha subo a una rama en particular no afecta el funcionamiento del producto.

Una vez realizadas estas verificaciones, arreglos y posteriores chequeos, se procede a realizar un merge con la rama stable o master para actualizar los cambios.

Por razones de tiempo no me dío oportunidad de continuar con esta prueba, entrego mi trabajo y documentación hasta el punto #2, he implementado lo que tengo en conocimiento inmediato que he adquirido durante mis labores como desarrollador de soporte IT.

Dispuesto a aprender sobre los demas retos, si bien puedo decir que conozco la tecnología, no he trabajado tanto con tecnologias como Kubernetes o Terraform como para decir que tengo un manejo excelente de estas.

Nuevamente, un cordial saludo, agradecido por brindarme la oportunidad de demostrar mis conocimientos.

*Nota: he especificado las versiones de los programas que instale a nivel local para poder ejecutar todo de manera correcta, ya que con versiones anteriores de estos paquetes se generaron algunos problemas durante la ejecución de los retos.

*Nota: Para ejecutar la aplicación basta con correr el siguiente comando

sudo docker-compose -f docker-compose.yml up --build

Se debe asegurar primero de que no existen aplicaciones o contenedores anteriores usando los puertos 3000, 80 y 443 para que el servidor nginx no genere error, en caso de presentar algun error adicional, este se mostrara por el debug en la consola donde se ejecute este comando.

Asegurese de ejecutar

sudo apt update
sudo apt upgrade

Para mantener actualizados todos los componentes core del sistema linux y evitar errores por falta de complemenntos.